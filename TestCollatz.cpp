// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

// added tests
TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(1500, 10500)), make_tuple(1500, 10500, 262));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(23532, 7854)), make_tuple(23532, 7854, 282));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(1, 999)), make_tuple(1, 999, 179));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(500, 1600)), make_tuple(500, 1600, 182));
}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(1, 1)), make_tuple(1, 1, 1));
}

TEST(CollatzFixture, eval9) {
    ASSERT_EQ(collatz_eval(make_pair(1, 999999)), make_tuple(1, 999999, 525));
}

TEST(CollatzFixture, eval10) {
    ASSERT_EQ(collatz_eval(make_pair(999999, 1)), make_tuple(999999, 1, 525));
}

TEST(CollatzFixture, eval11) {
    ASSERT_EQ(collatz_eval(make_pair(832981, 720448)), make_tuple(832981, 720448, 468));
}

TEST(CollatzFixture, eval12) {
    ASSERT_EQ(collatz_eval(make_pair(421729, 86874)), make_tuple(421729, 86874, 449));
}

TEST(CollatzFixture, eval13) {
    ASSERT_EQ(collatz_eval(make_pair(398164, 727685)), make_tuple(398164, 727685, 509));
}

TEST(CollatzFixture, eval14) {
    ASSERT_EQ(collatz_eval(make_pair(714632, 981065)), make_tuple(714632, 981065, 525));
}

TEST(CollatzFixture, eval15) {
    ASSERT_EQ(collatz_eval(make_pair(64605, 451999)), make_tuple(64605, 451999, 449));
}

TEST(CollatzFixture, eval16) {
    ASSERT_EQ(collatz_eval(make_pair(733746, 40231)), make_tuple(733746, 40231, 509));
}

TEST(CollatzFixture, eval17) {
    ASSERT_EQ(collatz_eval(make_pair(166589, 75164)), make_tuple(166589, 75164, 383));
}

TEST(CollatzFixture, eval18) {
    ASSERT_EQ(collatz_eval(make_pair(280391, 346123)), make_tuple(280391, 346123, 441));
}

TEST(CollatzFixture, eval19) {
    ASSERT_EQ(collatz_eval(make_pair(266744, 651777)), make_tuple(266744, 651777, 509));
}

TEST(CollatzFixture, eval20) {
    ASSERT_EQ(collatz_eval(make_pair(324635, 188662)), make_tuple(324635, 188662, 443));
}

// -----
// collatz_calc
// -----

TEST(CollatzFixture, calc0) {
    ASSERT_EQ(collatz_calc(510334, 692508), 509);
}

TEST(CollatzFixture, calc1) {
    ASSERT_EQ(collatz_calc(399135, 867588), 525);
}

TEST(CollatzFixture, calc2) {
    ASSERT_EQ(collatz_calc(582548, 800559), 509);
}

TEST(CollatzFixture, calc3) {
    ASSERT_EQ(collatz_calc(726518, 904248), 525);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}

// added tests
TEST(CollatzFixture, solve1) {
    istringstream sin("8736 32\n999999 999999\n1 1\n938472 47276\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "8736 32 262\n999999 999999 259\n1 1 1\n938472 47276 525\n");
}
